package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id=135209
 * Responses must be provided in JSON.
 */

public class JSONResponseHandlerEventsLast
{
    private static final String TAG = JSONResponseHandlerEventsLast.class.getSimpleName();
    private Team team;
    private Match match;

    /**
     * @param team
     */
    public JSONResponseHandlerEventsLast(Team team)
    {
        this.team = team;
    }

    /**
     * @return
     */
    public Team getTeam()
    {
        return team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException
    {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try
        {
            readEventsLast(reader);
        }
        finally
        {
            reader.close();
        }
    }

    /**
     *
     * @param reader
     * @throws IOException
     */
    public void readEventsLast(JsonReader reader) throws IOException
    {
        reader.beginObject();
        while (reader.hasNext())
        {
            String name = reader.nextName();
            if (name.equals("table"))
            {
                readArrayEventsLast(reader);
            }
            else
            {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    /**
     * @param reader
     * @throws IOException
     */
    private void readArrayEventsLast(JsonReader reader) throws IOException
    {
        reader.beginArray();
        int nb = 0;
        while (reader.hasNext())
        {
            reader.beginObject();
            while (reader.hasNext())
            {
                String name = reader.nextName();
                if (nb >= 0)
                {
                    if (name.equals("idEvent"))
                    {
                        match.setId(Long.valueOf(reader.nextString()));
                    }
                    else if (name.equals("strEvent"))
                    {
                        match.setLabel(reader.nextString());
                    }
                    else if (name.equals("strHomeTeam"))
                    {
                        match.setHomeTeam(reader.nextString());
                    }
                    else if (name.equals("strAwayTeam"))
                    {
                        match.setAwayTeam(reader.nextString());
                    }
                    else if (name.equals("intHomeScore"))
                    {
                        JsonToken jsonToken = reader.peek();
                        if (jsonToken == JsonToken.NULL)
                        {
                            match.setHomeScore(0);
                            reader.skipValue();
                        }
                        else
                        {
                            match.setHomeScore(reader.nextInt());
                        }
                    }
                    else if (name.equals("intAwayScore"))
                    {
                        JsonToken jsonToken = reader.peek();
                        if (jsonToken == JsonToken.NULL)
                        {
                            match.setAwayScore(0);
                            reader.skipValue();
                        }
                        else
                        {
                            match.setAwayScore(reader.nextInt());
                        }
                    }
                    else
                    {
                        reader.skipValue();
                    }
                }
                else
                {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}