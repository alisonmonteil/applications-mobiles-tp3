package com.example.tp3;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Classe qui hérite d'une AsyncTask pour effectuer des tâches de fond et de publier des résultats sans manipuler des threads
 */
public class LogoGetData extends AsyncTask<String, Void, Void>
{
    ImageView imageBadge;
    Activity activity;

    /**
     * @param image
     * @param activity
     */
    public LogoGetData(ImageView image, Activity activity)
    {
        this.imageBadge = image;
        this.activity = activity;
    }

    /**
     * Méthode qui permet d'effectuer la tâche de fond
     * @param strings
     * @return
     */
    @Override
    protected Void doInBackground(String... strings)
    {
        InputStream inputStream;
        Bitmap bitmap = null;
        try
        {
            inputStream = (InputStream) new URL(strings[0]).getContent();
            bitmap = BitmapFactory.decodeStream(inputStream);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        final Bitmap bit = bitmap;
        this.activity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                imageBadge.setImageBitmap(bit);
            }
        });
        return null;
    }
}