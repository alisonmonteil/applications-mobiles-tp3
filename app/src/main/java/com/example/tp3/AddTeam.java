package com.example.tp3;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Permet d'ajouter une nouvelle équipe à la RecyclerView
 */
public class AddTeam extends AppCompatActivity
{
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_team);

        /**
         * On appelle la fonction ici pour ajouter une nouvelle équipe
         */
        addData();
    }

    /**
     * Méthode qui permet d'ajouter une nouvelle équipe
     */
    public void addData()
    {
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /**
                 * On récupère les données écrites dans les textview
                 */
                TextView name = findViewById(R.id.editNewName);
                TextView league = findViewById(R.id.editNewLeague);

                /**
                 * On caste les données récupérées en String (de base)
                 */
                String currentNom = String.valueOf(name.getText());
                String currentLeague = String.valueOf(league.getText());

                /**
                 * On vérifie si tous les champs sont bien remplis, si oui on rentre dans la condition
                 */
                if (currentNom.length() != 0 && currentLeague.length() != 0)
                {
                    Team team = new Team(currentLeague, currentLeague);
                    SportDbHelper sportDbHelper = new SportDbHelper(AddTeam.this);
                    sportDbHelper.addTeam(team);
                    RecyclerViewAdapter.get(getApplicationContext(), sportDbHelper, AddTeam.this).getList().add(team);
                    RecyclerViewAdapter.get(getApplicationContext(), sportDbHelper, AddTeam.this).notifyDataSetChanged();
                    finish();
                }
                /**
                 * Si le champ n'est pas rempli, on affiche un message d'alerte
                 */
                else
                {
                    Toast.makeText(AddTeam.this, "Sauvegarde impossible", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}