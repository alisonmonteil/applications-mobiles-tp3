package com.example.tp3;

import android.os.Parcel;
import android.os.Parcelable;

public class Match implements Parcelable
{
    public static final String TAG = Match.class.getSimpleName();
    private long id;
    private String label;
    private String homeTeam;
    private String awayTeam;
    private int homeScore;
    private int awayScore;

    public Match()
    {

    }

    /**
     * @param id
     * @param label
     * @param homeTeam
     * @param awayTeam
     * @param homeScore
     * @param awayScore
     */
    public Match(long id, String label, String homeTeam, String awayTeam, int homeScore, int awayScore)
    {
        this.id = id;
        this.label = label;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

    /**
     * @param in
     */
    protected Match(Parcel in)
    {
        id = in.readLong();
        label = in.readString();
        homeTeam = in.readString();
        awayTeam = in.readString();
        homeScore = in.readInt();
        awayScore = in.readInt();
    }

    public static final Creator<Match> CREATOR = new Creator<Match>()
    {
        /**
         * @param in
         * @return
         */
        @Override
        public Match createFromParcel(Parcel in)
        {
            return new Match(in);
        }

        /**
         * @param size
         * @return
         */
        @Override
        public Match[] newArray(int size)
        {
            return new Match[size];
        }
    };

    /**
     * @return
     */
    public long getId()
    {
        return id;
    }

    /**
     * @param id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * @return
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * @param label
     */
    public void setLabel(String label)
    {
        this.label = label;
    }

    /**
     * @return
     */
    public String getHomeTeam()
    {
        return homeTeam;
    }

    /**
     * @param homeTeam
     */
    public void setHomeTeam(String homeTeam)
    {
        this.homeTeam = homeTeam;
    }

    /**
     * @return
     */
    public String getAwayTeam()
    {
        return awayTeam;
    }

    /**
     * @param awayTeam
     */
    public void setAwayTeam(String awayTeam)
    {
        this.awayTeam = awayTeam;
    }

    /**
     * @return
     */
    public int getHomeScore()
    {
        return homeScore;
    }

    /**
     * @param homeScore
     */
    public void setHomeScore(int homeScore)
    {
        this.homeScore = homeScore;
    }

    /**
     * @return
     */
    public int getAwayScore()
    {
        return awayScore;
    }

    /**
     * @param awayScore
     */
    public void setAwayScore(int awayScore)
    {
        this.awayScore = awayScore;
    }

    /**
     * @return
     */
    public String toString()
    {
        return label + " : "+homeScore+"-"+awayScore;
    }

    /**
     * @return
     */
    @Override
    public int describeContents()
    {
        return 0;
    }

    /**
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(label);
        dest.writeString(homeTeam);
        dest.writeString(awayTeam);
        dest.writeInt(homeScore);
        dest.writeInt(awayScore);
    }
}
