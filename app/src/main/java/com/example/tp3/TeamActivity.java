package com.example.tp3;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TeamActivity extends AppCompatActivity
{
    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = getIntent().getExtras().getParcelable("team");

        textTeamName = findViewById(R.id.nameTeam);
        textLeague = findViewById(R.id.league);
        textStadium = findViewById(R.id.editStadium);
        textStadiumLocation = findViewById(R.id.editStadiumLocation);
        textTotalScore = findViewById(R.id.editTotalScore);
        textRanking = findViewById(R.id.editRanking);
        textLastMatch = findViewById(R.id.editLastMatch);
        textLastUpdate = findViewById(R.id.editLastUpdate);
        imageBadge = findViewById(R.id.imageView);
        updateView();

        final Button but = findViewById(R.id.button);
        but.setOnClickListener(new View.OnClickListener()
        {
            /**
             * @param v
             */
            @Override
            public void onClick(View v)
            {
                int i = getIntent().getIntExtra("i",0);
                new GetData(TeamActivity.this, i, TeamActivity.this).execute(team);
                SportDbHelper sportDbHelper = new SportDbHelper(TeamActivity.this);
                RecyclerViewAdapter.get(TeamActivity.this, sportDbHelper, TeamActivity.this).notifyDataSetChanged();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent();
        intent.putExtra("Team", team);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    void updateView()
    {
        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        new LogoGetData(imageBadge,this).execute(team.getTeamBadge());
    }
}