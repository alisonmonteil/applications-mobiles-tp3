package com.example.tp3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
{
    private Context currentContext;
    private static ArrayList<Team> listTeam = new ArrayList<>();
    private SportDbHelper sportDbHelper;
    private Activity activity;
    private static RecyclerViewAdapter singleton;

    /**
     * @param context
     * @param sport
     * @param currentActivity
     */
    public RecyclerViewAdapter(Context context, SportDbHelper sport, Activity currentActivity)
    {
        currentContext = context;
        sportDbHelper = sport;
        activity = currentActivity;
        Cursor cursor = sport.fetchAllTeams();
        while(!cursor.isAfterLast())
        {
            listTeam.add(SportDbHelper.cursorToTeam(cursor));
            cursor.moveToNext();
        }
    }

    /**
     * @param context
     * @param sport
     * @param activity
     * @return
     */
    public static RecyclerViewAdapter get(Context context, SportDbHelper sport, Activity activity)
    {
        if(singleton == null)
        {
            singleton = new RecyclerViewAdapter(context, sport, activity);
        }
        return singleton;
    }

    /**
     * @return
     */
    public static ArrayList<Team> getList()
    {
        return listTeam;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    /**
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position)
    {
        new LogoGetData(holder.image, activity).execute(listTeam.get(position).getTeamBadge());
        holder.nom.setText(listTeam.get(position).getName());
        holder.league.setText(listTeam.get(position).getLeague());
        holder.rowId.setOnClickListener(new View.OnClickListener()
        {
            /**
             * @param view
             */
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(currentContext, TeamActivity.class);
                intent.putExtra("team", listTeam.get(position));
                intent.putExtra("position", position);
                currentContext.startActivity(intent);
            }
        });
    }

    /**
     * @return
     */
    @Override
    public int getItemCount()
    {
        return listTeam.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView nom;
        TextView league;
        ConstraintLayout rowId;
        ImageView image;

        /**
         * @param itemView
         */
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            nom = itemView.findViewById(R.id.name);
            league = itemView.findViewById(R.id.nameLeague);
            rowId = itemView.findViewById(R.id.rowId);
            image = itemView.findViewById(R.id.image);
        }
    }
}