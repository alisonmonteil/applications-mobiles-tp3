package com.example.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=4414&s=1920
 * Responses must be provided in JSON.
 */

public class JSONResponseHandlerLookupTable
{
    private static final String TAG = JSONResponseHandlerLookupTable.class.getSimpleName();
    private Team team;
    int integer;

    /**
     * @param team
     */
    public JSONResponseHandlerLookupTable(Team team)
    {
        this.team = team;
    }

    /**
     * @return
     */
    public Team getTeam()
    {
        return team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException
    {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try
        {
            readLookupTable(reader);
        }
        finally
        {
            reader.close();
        }
    }

    /**
     * @param reader
     * @throws IOException
     */
    public void readLookupTable(JsonReader reader) throws IOException
    {
        reader.beginObject();
        while (reader.hasNext())
        {
            String name = reader.nextName();
            if (name.equals("table"))
            {
                readArrayLookupTable(reader);
            }
            else
            {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    /**
     * @param reader
     * @throws IOException
     */
    private void readArrayLookupTable(JsonReader reader) throws IOException
    {
        reader.beginArray();
        int nb = 0;
        integer = 0;
        while (reader.hasNext())
        {
            reader.beginObject();
            while (reader.hasNext())
            {
                String name = reader.nextName();
                if (nb>=0)
                {
                    if (name.equals("teamid"))
                    {
                        long id = reader.nextLong();
                        if (id == team.getIdTeam())
                        {
                            team.setIdTeam(id);
                            team.setRanking(nb + 1);
                            integer = nb + 1;
                        }
                    }
                    else if (name.equals("total") && integer!=0)
                    {
                        team.setTotalPoints(reader.nextInt());
                        integer = 0;
                    }
                    else
                    {
                        reader.skipValue();
                    }
                }
                else
                {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}