package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    public static SportDbHelper sportDbHelper;
    public static SwipeRefreshLayout swipe;
    public SimpleCursorAdapter adapter = null;
    public RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initRecyclerView();

        swipe = findViewById(R.id.swipe);
        swipe.setColorSchemeColors(getResources().getColor(android.R.color.holo_blue_bright), getResources().getColor(android.R.color.holo_green_light), getResources().getColor(android.R.color.holo_orange_light), getResources().getColor(android.R.color.holo_red_light));

        /**
         * Permet d'actualiser la base de données en faisant un balayage vertical
         */
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                for (int i = 0; i < RecyclerViewAdapter.getList().size(); i++)
                {
                    new GetData(MainActivity.this, i, MainActivity.this).execute(RecyclerViewAdapter.getList().get(i));
                    SportDbHelper sportDbHelper = new SportDbHelper(MainActivity.this);
                    RecyclerViewAdapter.get(MainActivity.this, sportDbHelper,MainActivity.this).notifyDataSetChanged();
                }
                swipe.setRefreshing(false);
            }
        });

        /**
         * On crée un SimpleCursorAdapter qui permettra de lier le curseur à la vue d'adaptateur à l'aide
         * de la présentation de la vue d'élement de liste (avec 2 éléments)
         */
        adapter = new SimpleCursorAdapter(this,
            android.R.layout.simple_list_item_2,
            sportDbHelper.fetchAllTeams(),
            new String[] {
                    SportDbHelper.COLUMN_TEAM_NAME,
                    SportDbHelper.COLUMN_LEAGUE_NAME},
            new int[] {
                    android.R.id.text1,
                    android.R.id.text2});

        RecyclerView recyclerview = findViewById(R.id.recyclerView);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerview.setAdapter(RecyclerViewAdapter.get(this, sportDbHelper, MainActivity.this));

        /**
         * Permet d'ajouter une nouvelle équipe à la liste
         */
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MainActivity.this, AddTeam.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Méthode qui permet d'initialiser la Recycler View
     */
    private void initRecyclerView()
    {
        recyclerView = findViewById(R.id.recyclerView);
        sportDbHelper = new SportDbHelper(this);
        Cursor cursor = sportDbHelper.fetchAllTeams();
        if (cursor.getCount() == 0)
        {
            sportDbHelper.populate();
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(RecyclerViewAdapter.get(this, sportDbHelper, MainActivity.this));
    }

    /**
     * Permet de supprimer une équipe en faisant un balayage horizontal
     */
    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT)
    {
        /**
         * @param recyclerView
         * @param viewHolder
         * @param target
         * @return
         */
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        /**
         * @param viewHolder
         * @param direction
         */
        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            sportDbHelper.deleteTeam((int) RecyclerViewAdapter.getList().get(viewHolder.getAdapterPosition()).getId());
            RecyclerViewAdapter.getList().remove(viewHolder.getAdapterPosition());
            RecyclerViewAdapter.get(MainActivity.this, sportDbHelper, MainActivity.this).notifyDataSetChanged();
            Toast.makeText(MainActivity.this, "Équipe supprimée", Toast.LENGTH_LONG).show();
        }
    };

    /**
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
