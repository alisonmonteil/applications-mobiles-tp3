package com.example.tp3;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Classe qui hérite d'une AsyncTask pour effectuer des tâches de fond et de publier des résultats sans manipuler des threads
 */
public class GetData extends AsyncTask<Team, Void, Team>
{
    private Context currentContext;
    private int i;
    private Activity activity;

    /**
     * @param context
     * @param j
     * @param activity
     */
    public GetData(Context context, int j, Activity activity)
    {
        this.currentContext = context;
        this.i = j;
        this.activity = activity;
    }

    /**
     * Méthode qui permet d'effectuer la tâche de fond
     * @param team
     * @return
     */
    protected Team doInBackground(Team... team)
    {
        HttpURLConnection urlConnectionSearchTeam = null;
        HttpURLConnection urlConnectionSearchRanking = null;
        HttpURLConnection urlConnectionSearchMatch = null;

        try
        {
            /**
             * On prépare l'appel au service web
             * On appelle le service web
             * On initialise l'objet JSONResponseHandlerTeam*
             * On récupère les données du service web
             */
            URL urlSearchTeam;
            urlSearchTeam = WebServiceUrl.buildSearchTeam(team[0].getName());
            urlConnectionSearchTeam = (HttpURLConnection) urlSearchTeam.openConnection();
            InputStream inputStreamTeam = urlConnectionSearchTeam.getInputStream();
            JSONResponseHandlerTeam JsonSearchTeam = new JSONResponseHandlerTeam(team[0]);
            JsonSearchTeam.readJsonStream(inputStreamTeam);

            /**
             * On prépare l'appel au service web
             * On appelle le service web
             * On initialise l'objet JSONResponseHandlerLookupTable
             * On récupère les données du service web
             */
            URL urlSearchRanking;
            urlSearchRanking = WebServiceUrl.buildGetRanking(JsonSearchTeam.getTeam().getIdLeague());
            urlConnectionSearchRanking = (HttpURLConnection) urlSearchRanking.openConnection();
            InputStream inputStreamRanking = urlConnectionSearchRanking.getInputStream();
            JSONResponseHandlerLookupTable JsonSearchRanking = new JSONResponseHandlerLookupTable(JsonSearchTeam.getTeam());
            JsonSearchRanking.readJsonStream(inputStreamRanking);

            /**
             * On prépare l'appel au service web
             * On appelle le service web
             * On initialise l'objet JSONResponseHandlerEventsLast
             * On récupère les données du service web
             */
            URL urlSearchMatch;
            urlSearchMatch = WebServiceUrl.buildSearchLastEvents(JsonSearchTeam.getTeam().getIdTeam());
            urlConnectionSearchMatch = (HttpURLConnection) urlSearchMatch.openConnection();
            InputStream inputStreamMatch = urlConnectionSearchMatch.getInputStream();
            JSONResponseHandlerEventsLast JsonSearchMatch = new JSONResponseHandlerEventsLast(JsonSearchRanking.getTeam());
            JsonSearchMatch.readJsonStream(inputStreamMatch);
            return JsonSearchMatch.getTeam();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
        finally
        {
            urlConnectionSearchTeam.disconnect();
            urlConnectionSearchRanking.disconnect();
            urlConnectionSearchMatch.disconnect();
        }
    }

    /**
     * Méthode qui est appelée après avoir fait la tâche de fond
     * @param team
     */
    protected void onPostExecute(Team team)
    {
        SportDbHelper sportDbHelper = new SportDbHelper(currentContext);
        sportDbHelper.updateTeam(team);
        RecyclerViewAdapter.getList().set(i, team);
        RecyclerViewAdapter.get(currentContext, sportDbHelper, activity).notifyDataSetChanged();
    }
}
