package com.example.tp3;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

public class Team implements Parcelable
{
    public static final String TAG = Team.class.getSimpleName();
    private long id; // used for the _id colum of the db helper

    private String name;
    private long idTeam; // used for web service
    private String league;
    private long idLeague;
    private String stadium;
    private String stadiumLocation;
    private String teamBadge;
    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    /**
     * @param name
     */
    public Team(String name)
    {
        this.name = name;
    }

    /**
     * @param name
     * @param league
     */
    public Team(String name, String league)
    {
        this.name = name;
        this.league= league;
    }

    /**
     * @param id
     * @param name
     * @param idTeam
     * @param league
     * @param idLeague
     * @param stadium
     * @param stadiumLocation
     * @param teamBadge
     * @param totalPoints
     * @param ranking
     * @param lastEvent
     * @param lastUpdate
     */
    public Team(long id, String name, long idTeam, String league, long idLeague, String stadium, String stadiumLocation, String teamBadge, int totalPoints, int ranking, Match lastEvent, String lastUpdate)
    {
        this.id = id;
        this.name = name;
        this.idTeam = idTeam;
        this.league = league;
        this.idLeague = idLeague;
        this.stadium = stadium;
        this.stadiumLocation = stadiumLocation;
        this.teamBadge = teamBadge;
        this.totalPoints = totalPoints;
        this.ranking = ranking;
        this.lastEvent = lastEvent;
        this.lastUpdate = lastUpdate;
    }

    /**
     * @return
     */
    public long getId()
    {
        return id;
    }

    /**
     * @return
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return
     */
    public long getIdTeam()
    {
        return idTeam;
    }

    /**
     * @return
     */
    public String getLeague()
    {
        return league;
    }

    /**
     * @return
     */
    public long getIdLeague()
    {
        return idLeague;
    }

    /**
     * @return
     */
    public Match getLastEvent()
    {
        return lastEvent;
    }

    /**
     * @return
     */
    public String getStadium()
    {
        return stadium;
    }

    /**
     * @return
     */
    public String getStadiumLocation()
    {
        return stadiumLocation;
    }

    /**
     * @return
     */
    public String getTeamBadge()
    {
        return teamBadge;
    }

    /**
     * @return
     */
    public int getTotalPoints()
    {
        return totalPoints;
    }

    /**
     * @return
     */
    public int getRanking()
    {
        return ranking;
    }

    /**
     * @return
     */
    public String getLastUpdate()
    {
        return lastUpdate;
    }

    /**
     * @param id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @param idTeam
     */
    public void setIdTeam(long idTeam)
    {
        this.idTeam = idTeam;
    }

    /**
     * @param league
     */
    public void setLeague(String league)
    {
        this.league = league;
    }

    /**
     * @param idLeague
     */
    public void setIdLeague(long idLeague)
    {
        this.idLeague = idLeague;
    }

    /**
     * @param lastEvent
     */
    public void setLastEvent(Match lastEvent)
    {
        this.lastEvent = lastEvent;
        setLastUpdate();
    }

    /**
     * @param stadium
     */
    public void setStadium(String stadium)
    {
        this.stadium = stadium;
    }

    /**
     * @param stadiumLocation
     */
    public void setStadiumLocation(String stadiumLocation)
    {
        this.stadiumLocation = stadiumLocation;
    }

    /**
     * @param teamBadge
     */
    public void setTeamBadge(String teamBadge)
    {
        this.teamBadge = teamBadge;
    }

    /**
     * @param totalPoints
     */
    public void setTotalPoints(int totalPoints)
    {
        this.totalPoints = totalPoints;
        setLastUpdate();
    }

    /**
     * @param ranking
     */
    public void setRanking(int ranking)
    {
        this.ranking = ranking;
        setLastUpdate();
    }

    private void setLastUpdate()
    {
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
        this.lastUpdate = dateFormat.format(currentTime);
    }

    /**
     * @return
     */
    @Override
    public String toString()
    {
        return this.name+"("+this.league +"): "+this.lastEvent +"°C";
    }

    /**
     * @return
     */
    @Override
    public int describeContents()
    {
        return 0;
    }

    /**
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeLong(idTeam);
        dest.writeString(league);
        dest.writeLong(idLeague);
        dest.writeString(stadium);
        dest.writeString(stadiumLocation);
        dest.writeString(teamBadge);
        dest.writeInt(totalPoints);
        dest.writeInt(ranking);
        if (lastEvent != null)
            lastEvent.writeToParcel(dest,flags);
        dest.writeString(lastUpdate);
    }

    public static final Creator<Team> CREATOR = new Creator<Team>()
    {
        /**
         * @param source
         * @return
         */
        @Override
        public Team createFromParcel(Parcel source)
        {
            return new Team(source);
        }

        /**
         * @param size
         * @return
         */
        @Override
        public Team[] newArray(int size)
        {
            return new Team[size];
        }
    };

    /**
     * @param in
     */
    public Team(Parcel in)
    {
        this.id = in.readLong();
        this.name = in.readString();
        this.idTeam = in.readLong();
        this.league = in.readString();
        this.idLeague = in.readLong();
        this.stadium = in.readString();
        this.stadiumLocation = in.readString();
        this.teamBadge = in.readString();
        this.totalPoints = in.readInt();
        this.ranking = in.readInt();
        this.lastEvent = Match.CREATOR.createFromParcel(in);
        this.lastUpdate = in.readString();
    }
}